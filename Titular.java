package model;

public class Titular {
	private String nome, cpf, rg;
	private int telefone;
	private Conta umaConta;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public int getTelefone() {
		return telefone;
	}
	public void setTelefone(int telefone) {
		this.telefone = telefone;
	}
	public Conta getUmaConta() {
		return umaConta;
	}
	public void setUmaConta(Conta umaConta) {
		this.umaConta = umaConta;
	}
	
}
