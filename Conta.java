package model;

public class Conta{
	protected double saldo;
	protected double deposita;
	protected double saca;
	
	public void setSaldo(double saldo){
        this.saldo = saldo;
    }
    
    public double getSaldo(){
        return saldo;
    }
    
    public void setDeposita(double deposita){
        this.deposita = deposita;
    }
    
    public double getDeposita(){
        return deposita;
    }
    
    public void setSaca(double saca){
        this.saca = saca;
    }
    
    public double getsaca(){
        return saca;
    }
	
}